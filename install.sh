#!/bin/bash
if [ ! -d "./src" ];then
	echo "Pas de rep src ? Il faut me lancer de la racine"
	exit 0
fi

mkdir -p src/server/src/github.com/croll/arkeo-server
mkdir -p src/web

git clone https://github.com/croll/arkeogis-server.git src/server/src/github.com/croll/arkeogis-server
git clone https://github.com/croll/arkeogis-web.git src/web

npm install
cd src/web && bower install
cd ../..

cd src/server/src/github.com/lukeroth/ && patch < ../../../gdal-nowarn.patch

echo "Vérifiez que le répertoire src/server/src/github.com/croll/arkeogis-server fait partie de votre GOPATH. Je pourrais faire la vérification, mais j'ai la flemme :)"
