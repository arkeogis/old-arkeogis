# Structure

Le code du serveur se trouve dans src/server.
Le code du client web dans src/web

# Gulp et helpers

Un watcher de gulp se charge de surveiller le code du client et du serveur et place le tout dans le répertoire dist.
Dans le cas du code client, les fichiers sass sont automatiquement "compilés". Les fichiers js/html/etc sont déplacés dans le répertoire de destination.
Pour le serveur, les fichiers go sont automatiquement compilés, le binaire est placé dans dist, la conf est copiée et le serveur est relancé.

Si le module "live-reload" est installé dans Chrome ou Firefox, la page est rechargée automatiquement lorsqu'un fichier du client OU du serveur est modifié.

En éxécutant "gulp release" les fichiers css et js sont minifiés et merge dans un unique fichier js et un unique fichier css.

Todo: Le faire aussi pour les modules installés avec bower.

# Installation system

- Packages system sur la machine de dev (a chopper avec apt-get ou pacman ou ... selon distrib)
git, nodejs, npm, go, postgresql, postgis, libgdal-dev, gdal-bin

- sur debian/ubuntu :
cd /usr/bin/ && ln -s nodejs node

- Packages npm system (a installer avec npm -g)
sudo npm install -g gulp
sudo npm install -g bower

# Dépendences, compilation, ...
- set de la variable d'environement GOPATH
export GOPATH=$PWD/src/server/

- Installer les deps npm pour le dev
npm install

- Installer les deps pour le client web
cd src/web && bower install

- Lancer gulp
gulp

# Création de dump de database de PROD :
- Faire d'abord un tunnel SSH (ne pas utiliser le shell créé ensuite):
ssh -L 5433:postgresqltwo.db.huma-num.fr:5432 arkeogis@arkeogis.org

- Faire le dump depuis sa machine locale
pg_dump -h localhost -p 5433 -U user_arkeogis -W -d arkeogisdb -Fc -b -v > arkeogis.psql

# Création database
- Faire en administrateur postgresql :
createuser --pwprompt user_arkeogis

psql arkeogis
create extension postgis;
create extension unaccent;
\q

createdb -O user_arkeogis arkeogis
pg_restore -d arkeogis /home/nicolas/arkeogis/arkeogis.psql
