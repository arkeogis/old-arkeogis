package main

import (
	arkeogis "github.com/croll/arkeogis-server"
)

func main() {
	arkeogis.Start()
}
